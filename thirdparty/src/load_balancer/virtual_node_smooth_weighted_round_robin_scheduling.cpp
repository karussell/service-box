﻿#include "load_balancer/load_balance.h"
#include <random>

namespace kratos {
namespace loadbalance {

std::int32_t vn_get_random_int32(std::int32_t a, std::int32_t b) {
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<std::int32_t> dist(a, b);
  return dist(mt);
}

struct VNSWRRSNode {
  VNSWRRSNode() = default;
  VNSWRRSNode(LoadBalanceNodeWeakPtr n) : lb_node(n), cur_weight(0) {
    effect_weight = lb_node.lock()->get_weight();
  }
  LoadBalanceNodeWeakPtr lb_node;
  std::int32_t cur_weight{0};
  std::int32_t effect_weight{0};
};

class VNSWRRSBalancer : public ILoadBalancer {
public:
  VNSWRRSBalancer() = default;
  virtual ~VNSWRRSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override;

  static auto get_name() -> const std::string {
    return "SMOOTH_ROUND_ROBIN_BALANCER";
  }
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<VNSWRRSBalancer>();
  }

private:
  auto get_swrr_next() -> void;
  auto get_virtual_nodes() -> void;

private:
  static bool vn_smooth_weight_round_robin_register_;
  std::int32_t last_select_index_{-1};
  std::int32_t server_count_{0};
  std::int32_t virtual_server_count_{0};
  std::int32_t last_select_virtual_index_{-1};
  std::int32_t tot_weight_{0};
  std::string name_{""};
  std::vector<VNSWRRSNode> nodes_;
  std::vector<std::int32_t> virtual_nodes_;
};

auto VNSWRRSBalancer::add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool {
  if (lbnode.expired()) {
    return false;
  }
  server_count_++;
  tot_weight_ += lbnode.lock()->get_weight();
  nodes_.emplace_back(VNSWRRSNode(lbnode));
  return true;
}

auto VNSWRRSBalancer::get_next(const std::string &/*ip*/)
    -> LoadBalanceNodeWeakPtr {
  if (server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  if (virtual_server_count_ == 0 ||
      last_select_virtual_index_ == virtual_server_count_ - 1) {
    get_virtual_nodes();
  }
  if (virtual_server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  if (last_select_virtual_index_ == -1) {
    last_select_virtual_index_ =
        vn_get_random_int32(0, virtual_server_count_ - 1);
  }
  last_select_virtual_index_ =
      (last_select_virtual_index_ + 1) % virtual_server_count_;
  auto index = virtual_nodes_[last_select_virtual_index_];
  return nodes_[index].lb_node;
}

auto VNSWRRSBalancer::clear() -> void {
  nodes_.clear();
  server_count_ = 0;
  last_select_index_ = -1;
  last_select_virtual_index_ = -1;
  virtual_server_count_ = 0;
  virtual_nodes_.clear();
}

auto VNSWRRSBalancer::reset_balancer() -> void {
  last_select_index_ = -1;
  last_select_virtual_index_ = -1;
  virtual_server_count_ = 0;
  virtual_nodes_.clear();
  for (auto &it : nodes_) {
    if (it.lb_node.expired()) {
      continue;
    }
    it.effect_weight = it.lb_node.lock()->get_weight();
  }
}

void VNSWRRSBalancer::get_virtual_nodes() {
  if (last_select_virtual_index_ == tot_weight_ - 1 &&
      virtual_server_count_ == tot_weight_) {
    return;
  }
  auto need_add_count = tot_weight_ - virtual_server_count_;
  if (need_add_count > server_count_) {
    need_add_count = server_count_;
  }

  for (std::int32_t i = 0; i < need_add_count; i++) {
    get_swrr_next();
  }
  virtual_server_count_ = (std::int32_t)virtual_nodes_.size();
}

void VNSWRRSBalancer::get_swrr_next() {
  std::int32_t total_weight = 0;
  for (std::int32_t i = 0; i < server_count_; i++) {
    auto &node = nodes_[i];
    if (node.lb_node.expired()) {
      continue;
    }
    auto weight = node.lb_node.lock()->get_weight();
    if (weight == 0) {
      continue;
    }
    node.cur_weight += node.effect_weight;
    total_weight += node.effect_weight;
    if (node.effect_weight < weight) {
      node.effect_weight++;
    }
    if (last_select_index_ == -1 ||
        node.cur_weight > nodes_[last_select_index_].cur_weight) {
      last_select_index_ = i;
    }
  }
  if (last_select_index_ == -1) {
    return;
  }
  nodes_[last_select_index_].cur_weight -= total_weight;
  virtual_nodes_.emplace_back(last_select_index_);
}

Registe_Balancer(
    VNSWRRSBalancer, vn_smooth_weight_round_robin_,
    BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling,
    VNSWRRSBalancer::creator)
} // namespace loadbalance
} // namespace kratos
