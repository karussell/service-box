﻿#include "load_balancer/load_balance.h"
#include <iostream>
namespace kratos {
namespace loadbalance {

auto BalancerFactory::get_balancer_map() -> BalancersMap & {
  static BalancersMap balancers_{};
  return balancers_;
}

auto BalancerFactory::register_balancer(BalancerMod mod, ICreateMethod t)
    -> bool {
  auto &balancers = get_balancer_map();
  auto it = balancers.find(std::underlying_type<BalancerMod>::type(mod));
  if (it == balancers.end()) {
    balancers[std::underlying_type<BalancerMod>::type(mod)] = t;
    return true;
  }
  return false;
}

auto BalancerFactory::create_balancer(BalancerMod mod)
    -> std::unique_ptr<ILoadBalancer> {
  auto &balancers = get_balancer_map();
  auto it = balancers.find(std::underlying_type<BalancerMod>::type(mod));
  if (it != balancers.end()) {
    return balancers[std::underlying_type<BalancerMod>::type(mod)]();
  }
  return nullptr;
}

} // namespace loadbalance
} // namespace kratos
