/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONFIG_IMPL_H
#define CONFIG_IMPL_H

#include "kconfig/interface/config.h"
#include <unordered_map>

class Grammar;
class Domain;

using namespace kconfig;

class ConfigImpl : public Config {
public:
  ConfigImpl();
  virtual ~ConfigImpl();
  virtual void load(const std::string &base, const std::string &file);
  virtual void reload(const std::string &base, const std::string &file);
  virtual void reload();
  virtual void unload();
  virtual bool has(const std::string &key);
  virtual int8_t getInt8(const std::string &key);
  virtual uint8_t getUint8(const std::string &key);
  virtual int16_t getInt16(const std::string &key);
  virtual uint16_t getUint16(const std::string &key);
  virtual int32_t getInt32(const std::string &key);
  virtual uint32_t getUint32(const std::string &key);
  virtual int64_t getInt64(const std::string &key);
  virtual uint64_t getUint64(const std::string &key);
  virtual float getFloat(const std::string &key);
  virtual double getDouble(const std::string &key);
  virtual const std::string &getString(const std::string &key);
  virtual bool getBool(const std::string &key);
  virtual Attribute *get(const std::string &key);
  virtual NumberAttribute *number(const std::string &key);
  virtual StringAttribute *string(const std::string &key);
  virtual ArrayAttribute *array(const std::string &key);
  virtual TableAttribute *table(const std::string &key);
  virtual ZoneAttribute *zone(const std::string &key);
  virtual BoolAttribute *boolean(const std::string &key);
  virtual bool register_function(const std::string &name,
                                 ConfigFunction func) override;
  virtual ConfigFunction get_function(const std::string &name) override;
  virtual Domain* get_root_domain() override;

  void serviceStart();
  void serviceStop();

private:
  Grammar *_grammar;
  Domain *_root;
  std::string _base;
  std::string _file;
  using FuncMap = std::unordered_map<std::string, ConfigFunction>;
  FuncMap _func_map;
};

#endif // CONFIG_IMPL_H
