#include "../framework/unittest.hh"
#include "detail/box_argument_impl.hh"

FIXTURE_BEGIN(test_argument)

CASE(TestArgument1) {
	kratos::argument::BoxArgumentImpl argument;
	const char* argv[] = {
		"box", "-c", "file"
	};
	ASSERT_TRUE(argument.parse(3, argv));
	ASSERT_TRUE(argument.get_config_file_path() == "file");
}

CASE(TestArgument2) {
	kratos::argument::BoxArgumentImpl argument;
	ASSERT_TRUE(argument.parse(1, nullptr));
	ASSERT_TRUE(argument.get_config_file_path() == "box.cfg");
	ASSERT_TRUE(argument.get_max_frame() == 50);
}

CASE(TestArgument3) {
	kratos::argument::BoxArgumentImpl argument;
	const char* argv[] = {
		"box", "-f", "10"
	};
	ASSERT_TRUE(argument.parse(3, argv));
	ASSERT_TRUE(argument.get_max_frame() == 10);
}

CASE(TestArgument4) {
	kratos::argument::BoxArgumentImpl argument;
	const char* argv[] = {
		"box", "--config=file"
	};
	ASSERT_TRUE(argument.parse(2, argv));
	ASSERT_TRUE(argument.get_config_file_path() == "file");
}

CASE(TestArgument5) {
	kratos::argument::BoxArgumentImpl argument;
	const char* argv[] = {
		"box", "--frame=10"
	};
	ASSERT_TRUE(argument.parse(2, argv));
	ASSERT_TRUE(argument.get_max_frame() == 10);
}

CASE(TestArgument6) {
	kratos::argument::BoxArgumentImpl argument;
	const char* argv[] = {
		"box", "--help"
	};
	ASSERT_TRUE(!argument.parse(2, argv));
	ASSERT_TRUE(argument.is_print_help());
	ASSERT_TRUE(argument.get_help_string().size() > 0);
}

FIXTURE_END(test_argument)
