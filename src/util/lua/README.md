# Lua服务

可以使用lua语言开发服务，用户编写的lua脚本将会被自动加载并运行，运行于ServiceBox的lua脚本服务与C++语言编写的服务有同等能力。用户通过使用ctx模块来使用服务容器暴露的功能。

# ctx

## ctx方法
1.  **sleep(ms)**

睡眠ms毫秒.

返回值: _无_ 

```
ctx:sleep(100)
```
2. **expire_at(intval, func)**

启动一个intval毫秒后到期的定时器，到期时会调用用户提供的func函数.

返回值:

 _1. nil失败_ 

 _2. 定时器ID，lua整数_ 


```
local time_id = ctx:expire_at(1000,
  function (id)
    print("timeout:"..id)
  end)
```

3. **expire_periodic(intval, func)**

启动一个周期定时器，定时器间隔为intval毫秒，每次到期时会调用用户提供的func函数.

返回值:

 _1. nil失败_ 

 _2. 定时器ID，lua整数_ 


```
local time_id = ctx:expire_periodic(1000,
  function (id)
    print("timeout:"..id)
  end)
```

4. **cancel(time_id)**

取消定时器

返回值: _无_ 


```
ctx:cancel(time_id)
```


5. **shutdown()**

关闭ServiceBox

返回值: _无_


```
ctx:shutdown()
```


6. **register_service(service_name)**

注册一个名字为service_name的服务到集群

返回值:

_1.  true 成功_ 

_2.  false 失败_ 


```
local ret = ctx:register_service("my_service")
```


7. **unregister_service(service_name)**

从集群取消注册一个名字为service_name的服务

返回值:

_1. true 成功_

_2. false 失败_


```
ctx:unregister_service("my_service")
```


8. **log_verb(s)**

写日志，日志等级为VERBOSE

返回值: _无_


```
ctx:log_verb("i am a log line")
```

9. **log_info(s)**

写日志，日志等级为INFORMATION

返回值: _无_

```
ctx:log_info("i am a log line")
```

10. **log_debug(s)**

写日志，日志等级为DEBUG

返回值: _无_

```
ctx:log_debug("i am a log line")
```

11. **log_warn(s)**

写日志，日志等级为WARNING

返回值: _无_

```
ctx:log_warn("i am a log line")
```

12. **log_except(s)**

写日志，日志等级为EXCEPTION

返回值: _无_

```
ctx:log_except("i am a log line")
```

13. **log_fail(s)**

写日志，日志等级为FAILURE

返回值: _无_

```
ctx:log_fail("i am a log line")
```

14. **log_fatal(s)**

写日志，日志等级为FATAL

返回值: _无_

```
ctx:log_fatal("i am a log line")
```
## Proxy模块

代理，代表集群内或客户端服务的一个本地代理调用者.

1. **get_{服务名}_timeout(service_name, timeout)**

获取名字为service_name的服务代理，获取超时为timeout毫秒

返回值:

_1. nil 错误_

_2. 代理_


```
locall proxy = Proxy:get_Login_timeout("login_service", 1000)
local user_id = proxy.login("user", "pass")
```

2. **get_{服务名}_from_peer()**

从客户端获取服务代理, 方法不检测对端是否真的存在这个服务

返回值:

_1. nil 错误_

_2. 代理_


```
locall proxy = Proxy:get_MyService_from_peer()
proxy.some_method("hello")
```

3. **get_{服务名}_from_transport()**

从本次调用来源的网络管道获取服务代理, 方法不检测对端是否真的存在这个服务

返回值:

_1. nil 错误_

_2. 代理_

```
locall proxy = Proxy:get_MyService_from_transport()
proxy.some_method("hello")
```

# ctx模块

## argument模块

1. **get_config_file_path()**

获取ServiceBox配置文件路径

返回值: _配置文件路径_

```
local path = ctx.argument:get_config_file_path()
```

2. **get_config_file_name()**

获取ServiceBox配置文件名

返回值: _配置文件名_

```
local file_name = ctx.argument:get_config_file_name()
```

3. **get_max_frame()**

获取ServiceBox逻辑主循环当前帧/秒

返回值: _逻辑主循环当前帧/秒_

```
local frame = ctx.argument:get_max_frame()
```

4. **is_daemon()**

获取ServiceBox是否以后台进程的方式启动

返回值:

_1. true 是_

_2. false 否_

```
local is_daemon = ctx.argument:is_daemon()
```
5. **get_config_center_api_url()**

获取配置中心API URL, 服务容器启动后会去配置中心拉取配置文件，并使用这个配置文件启动容器

返回值: _获取配置中心API URL_

```
local url = ctx.argument:get_config_center_api_url()
```

6. **is_open_system_exception()**

ServiceBox是否将系统错误转换为C++异常处理, 以便可以被容器捕获

返回值:

_1. true 是_

_2. false 否_

```
local url = ctx.argument:is_open_system_exception()
```

## config模块

1. **get_listener_list()**

获取ServiceBox监听器地址列表

返回值: _lua数组，数组中的每一项为ServiceBox监听器地址_

```
local listeners = ctx.config:get_listener_list()
```

2. **get_service_finder_type()**

获取服务发现/注册集群类型

返回值: _服务发现/注册集群类型_

```
local type = ctx.config:get_service_finder_type()
```

3. **get_service_finder_hosts()**

获取服务发现/注册集群地址，字符串格式为: "ip:port,domain:port:..."

返回值: _服务发现/注册集群地址_

```
local type = ctx.config:get_service_finder_hosts()
```
4. **get_service_finder_connect_timeout()**

获取连接服务发现/注册集群超时时间，单位为秒

返回值: _连接服务发现/注册集群超时时间_

```
local type = ctx.config:get_service_finder_connect_timeout()
```
5. **get_necessary_service()**

获取ServiceBox启动必须依赖的远程服务，ServiceBox启动后将获取到所有必需的依赖服务才会完成启动流程，否则将一直处于等待状态

返回值: _依赖服务的名字数组_

```
local service_names = ctx.config:get_necessary_service()
```

6. **get_connect_other_box_timeout()**

获取当前ServiceBox主动连接其他ServerBox的连接超时，单位秒

返回值: _当前ServiceBox主动连接其他ServerBox的连接超时_

```
local timeout = ctx.config:get_connect_other_box_timeout()
```
7. **get_box_channel_recv_buffer_len()**

获取网络管道接收缓冲区初始长度，单位字节

返回值: _网络管道接收缓冲区初始长度_

```
local length = ctx.config:get_box_channel_recv_buffer_len()
```

8. **get_box_name()**

获取服务容器名称

返回值: _服务容器名称_

```
local name = ctx.config:get_box_name()
```

9. **get_logger_config_line()**

获取服务容器日志文件配置

返回值: _服务容器日志文件配置_

```
local log_config = ctx.config:get_logger_config_line()
```

10. **get_service_dir()**

获取本地服务Bundle存放的目录

返回值: _本地服务Bundle存放的目录_

```
local service_dir = ctx.config:get_service_dir()
```

11. **get_preload_service()**

获取预加载的本地服务Bundle表

返回值: _预加载的本地服务Bundle表_

```
local preload_table = ctx.config:get_preload_service()
```

12. **is_open_coroutine()**

ServiceBox是否开启了协程模式，这个协程模式与lua协程没有关联，是运行C++服务所使用的协程模式

返回值:

_1. true 是_

_2. false 否_

```
local is_open = ctx.config:is_open_coroutine()
```

13. **get_remote_service_repo_version_api()**

获取热更新版本文件RESTful API

返回值: _热更新RESTful API_

```
local version_api = ctx.config:get_remote_service_repo_version_api()
```

14. **get_remote_service_repo_dir()**

获取热更新远程仓库目录

返回值: _热更新远程仓库目录_

```
local remote_repo_dir = ctx.config:get_remote_service_repo_dir()
```
15. **get_remote_service_repo_latest_version_api()**

获取热更新最新版本文件RESTful API

返回值: _热更新RESTful API_

```
local version_api = ctx.config:get_remote_service_repo_latest_version_api()
```
16. **is_open_remote_update()**

是否开启了远程更新

返回值:

_1. true 是_

_2. false 否_

```
local is_open = ctx.config:is_open_remote_update()
```
17. **get_remote_repo_check_interval()**

获取热更新版本检查周期，单位秒

返回值: _热更新版本检查周期_

```
local check_intval = ctx.config:get_remote_repo_check_interval()
```

18. **is_start_as_daemon()**

是否以后台进程方式启动，优先级低于命令行参数，如果命令行参数配置了后台进程方式启动，则优先使用命令行配置

返回值:

_1. true 是_

_2. false 否_

```
local is_daemon = ctx.config:is_start_as_daemon()
```

19. **get_http_max_call_timeout()**

获取HTTP调用最大超时时间， 单位秒

返回值: _HTTP调用最大超时时间_

```
local timeout = ctx.config:get_http_max_call_timeout()
```

20. **is_open_rpc_stat()**

是否开启RPC统计

返回值:

_1. true 是_

_2. false 否_

```
local is_open = ctx.config:is_open_rpc_stat()
```

21. **has_attribute(attr_name)**

检测配置文件中是否配置了attr_name

返回值:

_1. true 是_

_2. false 否_


```
假设有配置内容：

myconf = {
    attr = "1"
}

lua代码:

local is_exists = ctx.config:has_attribute("myconf.attr")

```
22. **get_array(attr_name)**

获取配置属性attr_name对应的内容，类型必须为数组类型

返回值: 字符串数组


```
假设有配置内容：

myconf = {
    attr = ("1", "2", "3")
}

lua代码:

local array = ctx.config:get_array("myconf.attr")

```
23. **get_table(attr_name)**

获取配置属性attr_name对应的内容，类型必须为表

返回值: lua表，key,value均为字符串


```
假设有配置内容：

myconf = {
    attr = <a:"a", b:"b", c:"c">
}

lua代码:

local table = ctx.config:get_table("myconf.attr")

```

24. **get_string(attr_name)**

获取配置属性attr_name对应的内容，类型必须为字符串

返回值: 字符串


```
假设有配置内容：

myconf = {
    attr = "hello"
}

lua代码:

local s = ctx.config:get_string("myconf.attr")

```

25. **get_number(attr_name)**

获取配置属性attr_name对应的内容，类型必须为浮点数

返回值: 浮点数


```
假设有配置内容：

myconf = {
    attr = 2.3
}

lua代码:

local f = ctx.config:get_number("myconf.attr")

```

26. **get_integer(attr_name)**

获取配置属性attr_name对应的内容，类型必须为整数

返回值: 整数


```
假设有配置内容：

myconf = {
    attr = 5
}

lua代码:

local n = ctx.config:get_integer("myconf.attr")

```

27. **add_reload_listener(name, func)**

添加配置文件重载监听器，name为监听器名字，func为监听器回调

返回值:

_1. false 设置失败_

_2. true 设置成功_


```

local ret = ctx.config:add_reload_listener("listener_name",
function (listener_name)
  print("reloaded")
end)

```

27. **remove_reload_listener(name)**

删除配置文件重载监听器，name为监听器名字

返回值:

_1. false 设置失败_

_2. true 设置成功_


```

local ret = ctx.config:remove_reload_listener("listener_name")

```

## redis模块

1. **add_host(name, host, port, user, passwd)**

添加一个redis远程主机

name: 主机名<br>
host: 主机地址<br>
port: 主机监听端口<br>
user: redis用户名，没有则传空字符串<br>
passwd: redis密码, 没有则传空字符串

返回值:

_1. false 失败_

_2. true 成功_


```
local ret = ctx.redis:add_host("test", "127.0.0.1", 12345, "user", "passwd")
```

2. **start()**

启动redis模块

返回值:

_1. false 失败_

_2. true 成功_


```
local ret = ctx.redis:start()
```

3. **stop()**

关闭redis模块

返回值:

_1. false 失败_

_2. true 成功_


```
local ret = ctx.redis:stop()
```
4. **string_result(name, command, timeout)**

执行一条redis命令，要求返回结果必须是字符串

name: 主机名<br>
command: redis命令
timeout: 超时时间，毫秒

返回值:

_1. nil 发生错误_

_2. 有效值 成功_


```
local s = ctx.redis:string_result("test", "GET k", 1000)
```

5. **bool_result(name, command, timeout)**

执行一条redis命令，要求返回结果必须是bool

name: 主机名<br>
command: redis命令
timeout: 超时时间，毫秒

返回值:

_1. nil 发生错误_

_2. 有效值 成功_


```
local b = ctx.redis:bool_result("test", "SET k value", 1000)
```

6. **integer_result(name, command, timeout)**

执行一条redis命令，要求返回结果必须是整数

name: 主机名<br>
command: redis命令
timeout: 超时时间，毫秒

返回值:

_1. nil 发生错误_

_2. 有效值 成功_


```
local n = ctx.redis:integer_result("test", "GET k1", 1000)
```

7. **number_result(name, command, timeout)**

执行一条redis命令，要求返回结果必须是浮点数

name: 主机名<br>
command: redis命令
timeout: 超时时间，毫秒

返回值:

_1. nil 发生错误_

_2. 有效值 成功_


```
local f = ctx.redis:number_result("test", "GET d", 1000)
```

8. **array_result(name, command, timeout)**

执行一条redis命令，要求返回结果必须是数组

name: 主机名<br>
command: redis命令
timeout: 超时时间，毫秒

返回值:

_1. nil 发生错误_

_2. 有效值 成功_


```
local f = ctx.redis:array_result("test", "SMEMBERS sk", 1000)
```

9. **table_result(name, command, timeout)**

执行一条redis命令，要求返回结果必须是表

name: 主机名<br>
command: redis命令
timeout: 超时时间，毫秒

返回值:

_1. nil 发生错误_

_2. 有效值 成功_


```
local f = ctx.redis:table_result("test", "HGETALL hk", 1000)
```

## time模块

1. **get_millionsecond()**

获取当前毫秒数，自1970/01/01:00:00:00起

返回值: 当前毫秒数

```
local ms = ctx.time:get_millionsecond()
```

2. **get_second()**

获取当前秒数，自1970/01/01:00:00:00起

返回值: 当前秒数

```
local second = ctx.time:get_second()
```

3. **utc_diff_second()**

获取当前时区秒数与UTC之差，自1970/01/01:00:00:00起

返回值: 当前时区秒数与UTC之差

```
local diff_second = ctx.time:utc_diff_second()
```

4. **diff_days_now(now)**

获取当前时间与时间戳now相差的天数，时间戳单位为秒

返回值: 当前时间与时间戳now相差的天数

```
local diff_days = ctx.time:diff_days_now(t)
```

5. **diff_days(t1, t2)**

获取时间戳t1与时间戳t2相差的天数，时间戳单位为秒

返回值: 时间戳t1与时间戳t2相差的天数

```
local diff_days = ctx.time:diff_days(t1, t2)
```

6. **get_date()**

获取日期表

返回值: 

**表的键如下**:

year 年<br>
month 月<br>
day 日<br>
weekday 一周之内的第几天, [0,6], 0为周日，6为周一<br>
hour 小时<br>
minute 分钟<br>
second 秒<br>
elapsed_seconds 1970/1/1 00:00:00到现在流逝的秒数<br>
elapsed_days 1970/1/1 00:00:00 到现在流逝的天数<br>
next_weekday 当前日期本周内的下一天<br>
prev_weekday 当前日期本周内的上一天<br>
days_at_week 当前日期在一周内的第几天<br>
days_at_month 当前日期在一月内的第几天<br>
days_at_year 当前日期在一年内的第几天<br>
days_of_month 当前日期所在月有多少天<br>
days_of_year 当前日期所在年有多少天<br>
is_last_day_of_week 是否是本周最后一天, 周日<br>
is_last_day_of_month 是否是本月最后一天<br>
is_last_day_of_year 是否是本年最后一天<br>
```
local date = ctx.time:get_date()
```

7. **date_from_string(fmt)**

通过时间字符串获取date, fmt格式为：1970/01/01 00:00:00

返回值: 参考get_date()
```
local date = ctx.time:date_from_string("1990/01/01 00:00:00")
```
8. **data_from_time(t)**

通过时间戳获取date, t的单位为秒

返回值: 参考get_date()
```
local date = ctx.time:data_from_time(t)
```

9. **is_same_day(t1, t2)**

检查两个时间戳是否在同一天

返回值: 

_1. true 是_

_2. false 否_


```
local is_same = ctx.time:is_same_day(t1，t2)
```

10. **is_same_week(t1, t2)**

检查两个时间戳是否在同一周

返回值: 

_1. true 是_

_2. false 否_


```
local is_same = ctx.time:is_same_week(t1，t2)
```

11. **is_same_month(t1, t2)**

检查两个时间戳是否在同一月

返回值: 

_1. true 是_

_2. false 否_


```
local is_same = ctx.time:is_same_month(t1，t2)
```

## http模块

1. **wait_response(host, port, uri, method, header, content, timeout, cb)**

发起一个HTTP请求并等待返回

host 远程主机地址或域名, 字符串<br>
port 远程端口, 整数<br>
uri URI<br>, 字符串<br>
method HTTP请求方法，例如:GET, 字符串<br>
header 用户定义的header表<br>
content 请求内容, 字符串<br>
timeout 连接超时时间, 秒, 整数<br>
cb response回调, cb函数有一个参数，为response表<br>

**表的键为:**

host 如果存在则为远程主机地址或域名, 字符串<br>
port 如果存在则为远程端口, 整数<br>
header 如果存在则为header表<br>
content 如果存在则为内容, 字符串<br>
method 如果存在则为HTTP方法, 字符串<br>
status_code 如果存在则为HTTP状态码, 整数<br>
uri 如果存在则为URI, 字符串<br>

返回:

_1. nil 失败_

_2. true 成功_


```
function client_cb(response)
  print_r(response)
end
local ret = ctx.http:wait_response("127.0.0.1", 8888, "/", "GET", {test="value"}, "hello", 1000, client_cb)
```

2. **wait_request(host, port, cb)**

监听host:port等待HTTP请求并调用cb函数处理

cb函数的参数为request表， 内容请参见wait_response

返回:

_1. nil 失败_

_2. true 成功_

```
function server_cb(request)
  local response = {}
  response.content = "hello world"
  response.status_code = 200
  return response
end

local ret = ctx.http:wait_request("127.0.0.1", 8888, server_cb)
```

## console模块

1. **add_switch(name, tips, refresh_cb, change_cb)** 

添加一个设置开关,可以通过console进行设置

name 开关名字<br>
tips console上显示给用户看的提示<br>
refresh_cb console需要刷新数据时的回调<br>
change_cb console操作设置时的回调<br>

refresh_cb 没有参数，需要返回三个值，用于显示的名字， 开关状态(true或false), 最新的tips<br>
change_cb 有一个参数，是bool，表示当前开关的状态，需要返回二个值，成功或失败(true或false), 失败的描述

返回:

_1. nil 失败_

_2. true 成功_


```
function refresh_cb()
  return "display_name", true, "i am a tips"
end
function change_cb(on_off)
  return true, ""
end
local result = ctx.console:add_switch("name", "tips", refresh_cb, change_cb)
```

2. **add_selection(name, tips, refresh_cb, change_cb)**

添加一个选项，可以通过console进行设置

name 开关名字<br>
tips console上显示给用户看的提示<br>
refresh_cb console需要刷新数据时的回调<br>
change_cb console操作设置时的回调<br>

refresh_cb 没有参数，需要返回三个值，用于显示的名字, 选项字符串数组, 最新的tips<br>
change_cb 有一个参数，是字符串，表示当前被选中的选项名字，需要返回二个值，成功或失败(true或false), 失败的描述

返回:

_1. nil 失败_

_2. true 成功_


```
function refresh_cb()
  return "display_name", {"a", "b", "c"}, "i am a tips"
end
function change_cb(name)
  return true, ""
end

local result = ctx.console:add_selection("name", "tips", refresh_cb, change_cb)
```

