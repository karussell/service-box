#pragma once

#include <memory>

namespace kratos {
namespace util {

class TokenProducer;
class TokenConsumer;
class TokenBucket;
class TokenProducerFactory;

using TokenProducerPtr = std::shared_ptr<TokenProducer>;
using TokenConsumerPtr = std::shared_ptr<TokenConsumer>;
using TokenBucketPtr = std::shared_ptr<TokenBucket>;
using TokenProducerFactoryPtr = std::shared_ptr<TokenProducerFactory>;

} // namespace util
} // namespace kratos
