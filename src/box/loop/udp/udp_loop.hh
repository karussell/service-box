#pragma once

#include "util/box_std_allocator.hh"
#include "box/loop/loop.hh"
#include "udp_channel_loop.hh"

namespace kratos {
namespace service {
class BoxNetwork;
struct NetEventData;
} // namespace service
} // namespace kratos

namespace kratos {
namespace loop {

using ListenerNameMap =
    kratos::service::PoolUnorederedMap<std::uint64_t,
                                       std::pair<std::uint64_t, std::string>>;
/**
 * @brief TCP����ѭ��ʵ��
 */
class UdpLoop : public Loop {
  service::BoxNetwork *network_{nullptr};         ///< ������ָ��
  std::shared_ptr<network::UdpChannelLoop> loop_; ///< UDP����ѭ��

public:
  UdpLoop(service::BoxNetwork *network);
  virtual ~UdpLoop();
  virtual auto start() -> bool override;
  virtual auto stop() -> bool override;
  virtual auto worker_update() -> void override;
  virtual auto do_worker_event(const service::NetEventData &event_data)
      -> void override;

public:
  /**
   * @brief ��ȡ������
   * @return ������ָ��
   */
  auto get_network() -> service::BoxNetwork *;

private:
  auto do_listen_request(const service::NetEventData &event_data) -> void;
  auto do_connect_request(const service::NetEventData &event_data) -> void;
  auto do_send_request(const service::NetEventData &event_data) -> void;
  auto do_close_request(const service::NetEventData &event_data) -> void;
  auto on_accept(std::uint64_t channel_id, const std::string& name) -> void;
  auto on_receive(std::uint64_t channel_id, const char * data, std::uint32_t size) -> void;
};

} // namespace loop
} // namespace kratos
