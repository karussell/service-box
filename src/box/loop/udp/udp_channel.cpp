#include "udp_channel.hh"
#include "util/object_pool.hh"
#include "udp_os.hh"
#include "udp_socket.hh"
#include <iostream>

namespace kratos {
namespace network {

UdpChannel::UdpChannel(UdpType type) : socket_(nullptr), type_(type) {}

UdpChannel::~UdpChannel() {
  if (socket_) {
    box_dispose(socket_);
  }
}

void UdpChannel::update() {
  if (!socket_) {
    return;
  }
  socket_->update(1);
}

void UdpChannel::send(const char *data, std::size_t length) {
  if (socket_) {
    socket_->sendto(data, length, remoteAddress_.getAddress());
  }
}

UdpType UdpChannel::getType() const { return type_; }

UdpSocket &UdpChannel::getSocket() { return *socket_; }

UdpChannel::operator bool() { return (nullptr != socket_); }

void UdpChannel::close() {
  if (!socket_) {
    return;
  }
  box_dispose(socket_);
  socket_ = nullptr;
}

UdpAcceptorChannel::UdpAcceptorChannel() : UdpChannel(UdpType::Acceptor) {}

UdpAcceptorChannel::~UdpAcceptorChannel() {}

bool UdpAcceptorChannel::accept(const std::string &ip, std::uint16_t port) {
  auto socket = network::socket();
  SocketAddress address;
  if (!network::bind(socket, ip, port, address)) {
    return false;
  }
  if (socket > 0) {
    socket_ = allocate<UdpSocket>(socket);
  } else {
    return false;
  }
  type_ = UdpType::Acceptor;
  localAddress_.setAddress(ip, port);
  return true;
}

UdpAddress &UdpAcceptorChannel::getLocalAddress() { return localAddress_; }

UdpConnectorChannel::UdpConnectorChannel()
    : UdpChannel(UdpType::Connector), id_(0) {
  static std::uint64_t id = 1;
  id_ = id++;
}

UdpConnectorChannel::~UdpConnectorChannel() {}

bool UdpConnectorChannel::connect(const std::string &ip, std::uint16_t port) {
  auto socket = network::socket();
  SocketAddress address;
  if (!network::bind(socket, address)) {
    return false;
  }
  if (socket > 0) {
    socket_ = allocate<UdpSocket>(socket);
  } else {
    return false;
  }
  type_ = UdpType::Connector;
  remoteAddress_.setAddress(ip, port);
  return true;
}

UdpAddress &UdpConnectorChannel::getRemoteAddress() { return remoteAddress_; }

void UdpConnectorChannel::setRemoteAddress(const UdpAddress &address) {
  remoteAddress_ = address;
}

std::uint64_t UdpConnectorChannel::getID() { return id_; }

UdpRemoteChannel::UdpRemoteChannel(const UdpAddress &address, UdpSocket *socket)
    : UdpChannel(UdpType::Remote) {
  socket_ = socket;
  remoteAddress_ = address;
}

UdpRemoteChannel::~UdpRemoteChannel() {
  // 不要销毁, 由acceptor销毁
  socket_ = nullptr;
}

UdpAddress &UdpRemoteChannel::getRemoteAddress() { return remoteAddress_; }

} // namespace network
} // namespace kratos
