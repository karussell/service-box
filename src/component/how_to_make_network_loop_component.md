# Loop接口

Loop接口提供了某种网络通信的抽象，loop接口有两类方法:

1. 运行在主逻辑线程的方法
2. 运行在工作线程的方法

# 逻辑主线程与工作线程

主逻辑线程与工作线程通过队列通信, 队列内存放了事件，逻辑线程和工作线程按照固定的帧率(tick)从队列内取出事件并进行处理.

```

              |        |          |        |
              |        |          |        |
              |  e1 ------queue----> e1`   |
              |        |          |        |
              | main   |          |        |
              | thread |          |        |
              |        |          |        |
              |  e2` <-----queue------ e2  |
              |        |          |        |
              |        |          | worker | 
              |        |          | thread |
              |        |          |        |
  ```

## 处理逻辑主线程发送过来的事件

实现方法 `do_worker_event`来处理逻辑主线程发送过来的事件:

```
virtual auto do_worker_event(const service::NetEventData &event_data) = 0;
```
有四种类型的事件:
```
switch (event_data.event_id) {
  case service::NetEvent::listen_request:
    // 请求建立一个监听器
    break;
  case service::NetEvent::connect_request:
    // 请求建立一个连接器
    break;
  case service::NetEvent::send_data_notify:
    // 请求发送数据包
    break;
  case service::NetEvent::close_request:
    // 请求关闭虚拟管道
    break;
  }
```

### listen_request

定义:
```
struct ListenRequest {
  char *name = nullptr; // 唯一标识一个监听器名字
  char *host = nullptr; // 监听的IP
  int port; // 监听的端口
};
```

### connect_request
定义:
```
struct ConnectRequest {
  char *name = nullptr; // 唯一标识一个连接器名字
  char *host = nullptr; // 连接的目标IP
  int port; // 连接的目标端口
  int timeout; // 连接超时，秒
};
```

### send_data_notify

定义:
```
struct SendDataNotify {
  std::uint64_t channel_id; // 虚拟管道ID
  char *data_ptr = nullptr; // 需要发送的数据包地址
  int length; // 需要发送的数据包长度
};
```

### close_request
定义:
```
struct CloseRequest {
  std::uint64_t channel_id; // 虚拟管道ID
};
```

## 工作线程发送事件到主逻辑线程

BoxNetwork接口提供了如下几个方法，用来从工作线程发送事件到主逻辑线程:
```
virtual auto listen_response(const std::string &name,
                               std::uint64_t channel_id, bool success) -> bool;
virtual auto accept_notify(const std::string &name, std::uint64_t channel_id,
                           const std::string &local_ip, int local_port,
                           const std::string &peer_ip, int peer_port) -> bool;
virtual auto connect_response(const std::string &name,
                              std::uint64_t channel_id,
                              const std::string &local_ip, int local_port,
                              const std::string &peer_ip, int peer_port,
                              bool success) -> bool;
virtual auto recv_data_notify(std::uint64_t channel_id, char *data,
                              int length) -> bool;
virtual auto close_notify(std::uint64_t channel_id) -> bool;
```

以上函数返回`true`则意味着事件已经发送到主逻辑线程队列，返回`false`则发送失败.

### listen_response

对应事件listen_request的ACK事件, 监听器建立完成后调用，参数如下:
1. `name` listen_request内携带的name字段内容
2. `channel_id` 监听器虚拟管道ID, 不同的管道ID不能重复, 因为不同loop产生的管道ID使用统一的名字空间，应使用64位UUID，避免重复
3. `success` 是否成功

### accept_notify
当监听器有新的`连接`建立完成后调用，参数如下:
1. `name` listen_request内携带的name字段内容
2. `channel_id` 监听器虚拟管道ID, 不同的管道ID不能重复, 因为不同loop产生的管道ID使用统一的名字空间，应使用64位UUID，避免重复
3. `local_ip` 相对对端的本地IP
4. `local_port` 相对对端的本地端口
5. `peer_ip` 对端的IP
6. `peer_port` 对端的端口

### connect_response

对应事件connect_request的ACK事件, 连接器建立完成后调用，参数如下:
1. `name` connect_request内携带的name字段内容
2. `channel_id` 监听器虚拟管道ID, 不同的管道ID不能重复, 因为不同loop产生的管道ID使用统一的名字空间，应使用64位UUID，避免重复
3. `local_ip` 相对对端的本地IP
4. `local_port` 相对对端的本地端口
5. `peer_ip` 对端的IP
6. `peer_port` 对端的端口
7. `success` 是否成功

### recv_data_notify

当接收到数据时调用，参数如下:
1. `channel_id` 虚拟管道ID
2. `data` 接收到的数据指针
3. `length` 接收到的数据长度

### close_notify
当对端关闭时调用，参数如下:
1. `channel_id` 虚拟管道ID

# Loop::start
在loop建立后调用，用于自定义的初始化工作, **在逻辑主线程内调用**

# Loop::stop
在loop被销毁前调用，用于自定义的清理工作, **在逻辑主线程内调用**

# Loop::worker_update
周期性调用，loop的主循环, **在工作线程内调用**

# 实现LoopFactory接口

```
/**
 * Loop工厂
 */
class LoopFactory {
public:
  virtual ~LoopFactory() {}
  /**
   * 获取Loop类型
   */
  virtual auto get_type() -> const std::string & = 0;
  /**
   * 建立Loop
   * @return Loop实例
   */
  virtual auto create_loop() -> LoopPtr = 0;
};
```

1. `get_type` 返回一个标识loop类型的字符串，不同类型的loop不能重名
2. `create_loop`  创建一个loop实例

# 被ServiceBox加载
将自己实现的loop被ServiceBox加载，需要包装为[component](https://gitee.com/dennis-kk/service-box/blob/v0.3.0-alpha/src/component/how_to_make_component.md).

## 注册LoopFactory
在组件初始化函数`on_init`中注册LoopFactory.
```
FuncExport bool on_init(kratos::service::ServiceBox *box) {
  ......
  box->register_loop_factory("MyLoop", std::make_shared<MyLoopFactory>());
  ......
  return true;
}
```

## 配置
[ServiceBox运行时配置](https://gitee.com/dennis-kk/service-box/blob/v0.3.0-alpha/README-configuration.md)的监听器字段配置你的loop类型的监听器:
```
listener = {
    host = ("MyLoop:\\127.0.0.1:6888",...)
}
```
`MyLoop`是`LoopFactory::get_type`返回的类型, 默认为`tcp`.