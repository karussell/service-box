#include "service_finder/service_finder.hh"
#include "service_register/service_register.hh"
#include "zookeeper/service_finder_zookeeper.hh"
#include "zookeeper/service_register_zookeeper.hh"
#include "util/object_pool.hh"

namespace kratos {
namespace service {

std::shared_ptr<ServiceRegister> getRegister(const std::string &type) {
  if ("zookeeper" == type) {
    return make_shared_pool_ptr<ServiceRegisterZookeeper>();
  }
  return nullptr;
}

std::shared_ptr<ServiceFinder> getFinder(const std::string &type) {
  if ("zookeeper" == type) {
    return make_shared_pool_ptr<ServiceFinderZookeeper>();
  }
  return nullptr;
}

} // namespace service
} // namespace kratos
