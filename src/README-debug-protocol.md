# 脚本调试器协议

本文档说明脚本调试器的协议, 用来实现调试客户端, 客户端与服务器采用WebSocket连接，协议采用JSON格式。

# 请求-应答协议

## 协议通用结构

### 请求
必须包含如下字段。
```
{
    "command" : 命令名, 字符串
    "machine_name" : 虚拟机名称, 字符串 
}
```
### 响应
必须包含如下字段。
```
{
    "command" : 命令名, 字符串
    "machine_name" : 虚拟机名称, 字符串 
    "error": 错误描述，成功为ok, 其他则认为是有错误发生, 字符串
    "unique_id": 虚拟机协程ID, 没有则填0, 整数
}
```
## 请求-应答协议详述

### attach
附加到machine_name所指示的虚拟机, 同一时间一个客户端只能attach一个虚拟机。
```
1. 请求

特殊字段：无

2. 响应

特殊字段
{
    "machine_version": 虚拟机版本信息，字符串
}
```
### detach
从当前附加的虚拟机脱离。
```
1. 请求

特殊字段：无

2. 响应

特殊字段：无
```
### get_machine_list
获取所有运行的虚拟机列表。
```
1. 请求

特殊字段：无

2. 响应

特殊字段:

"result": [
    {
        "name":虚拟机名字, 字符串
        "enable":true或false,是否启用调试, 布尔
    },
    ...
]
```
### get_tree
获取虚拟机内运行的代码目录树。
```
1. 请求

特殊字段：无

2. 响应

特殊字段:

"tree": [
    文件名1,
    文件名2,
    {
        "name":目录名,
        "children":
        [
            文件名1,
            文件名2,
            {
                ...
            }
        ]
    }
]
```
### step_in
逐行运行并断点，遇到函数调用则进入函数，并断点在被调用函数的第一行。
```
1. 请求

特殊字段：无

2. 响应

特殊字段: 无

```
### step_out
跳出当前函数到函数外层，并断点在函数调用后一行。
```
1. 请求

特殊字段：无

2. 响应

特殊字段: 无

```
### step_over
逐行执行并断点，遇到调用不进入。
```
1. 请求

特殊字段：无

2. 响应

特殊字段: 无

```
### continue
继续执行，直到遇到断点。
```
1. 请求

特殊字段：无

2. 响应

特殊字段: 无

```
### get_breakpoint
获取所有已设置的断点。
```
1. 请求

特殊字段：无

2. 响应

特殊字段: 

{
   "bp":
    [
        {
            "file":文件名, 字符串
            "line":行号, 整数
            "break_id":断点ID, 字符串
            "state":"disable"或"enable", 字符串
        },
        ...
    ]
}

```
### add_breakpoint
添加一个断点。
```
1. 请求

特殊字段：
{
    "file":文件名,字符串
    "line":行号, 整数
}

2. 响应

特殊字段: 

{
   "file":文件名, 字符串
   "line":行号, 整数
   "break_id":断点ID, 字符串,   
   "bp":
    [
        {
            "file":文件名, 字符串
            "line":行号, 整数
            "break_id":断点ID, 字符串
            "state":"disable"或"enable", 字符串
        },
        ...
    ]
}

```
### remove_breakpoint
删除一个断点。
```
1. 请求

特殊字段：
{
    "break_id":断点ID,字符串
    "break_index":客户端用来标识断点的唯一ID, 整数
}

2. 响应

特殊字段: 

{
   "bp":
    [
        {
            "file":文件名, 字符串
            "line":行号, 整数
            "break_id":断点ID, 字符串
            "state":"disable"或"enable", 字符串
        },
        ...
    ]
}

```
### enable_breakpoint
启用一个或所有断点。
```
1. 请求

特殊字段：
{
    "break_id":断点ID,字符串
    "break_index":客户端用来标识断点的序号, 整数, 如果为-1则表示要开启所有断点
}

2. 响应

特殊字段: 

{
   "breakpoints":
    [
        {
            "break_id":断点ID, 字符串
            "break_index":客户端用来标识断点的序号, 整数
        },
        ...
    ]
}

```
### disable_breakpoint
禁用一个或所有断点。
```
1. 请求

特殊字段：
{
    "break_id":断点ID,字符串
    "break_index":客户端用来标识断点的序号, 整数, 如果为-1则表示要禁用所有断点
}

2. 响应

特殊字段: 

{
   "breakpoints":
    [
        {
            "break_id":断点ID, 字符串
            "break_index":客户端用来标识断点的序号, 整数
        },
        ...
    ]
}

```
### enable_debugger

启用调试器。
```
1. 请求

特殊字段：无

2. 响应

特殊字段: 无

```

### disable_debugger
禁用调试器。
```
1. 请求

特殊字段：无

2. 响应

特殊字段: 无

```
### upvalues

获取当前栈上所有upvalue
```
1. 请求

特殊字段：无

2. 响应

特殊字段:
{
    "content":值， 字符串，格式没有要求
}

```
### locals

获取当前栈上所有局部变量
```
1. 请求

特殊字段：无

2. 响应

特殊字段:
{
    "content": 值，字符串，格式没有要求
}

```
### list

获取当前断点代码行前后的源代码
```
1. 请求

特殊字段：无

2. 响应

特殊字段:
{
    "content": 代码，字符串，格式没有要求
}

```
### print

获取local或upvalue变量的值
```
1. 请求

特殊字段:
{
    "name":变量名，字符串
}

2. 响应

特殊字段:
{
    "content": 值，字符串，格式没有要求
}

```

### eval

执行一段代码
```
1. 请求

特殊字段:
{
    "code":代码，字符串
}

2. 响应

特殊字段:
{
    "content": 返回值，字符串，格式没有要求
}

```

### reload

重新加载脚本, 调试器将重新生成，包含断点在内的所有数据将被清除
```
1. 请求

特殊字段: 无

2. 响应

特殊字段: 无

```
### restart

重新加载脚本, 调试器将重新生成，除了断点意外的数据将被清除
```
1. 请求

特殊字段: 无

2. 响应

特殊字段: 无

```

### get_thread

获取所有线程/协程状态
```
1. 请求

特殊字段: 无

2. 响应

特殊字段:
{
    "content": 状态信息，字符串，格式无要求
}

```
### backtrace

获取当前断点处的调用栈信息
```
1. 请求

特殊字段: 无

2. 响应

特殊字段:
{
    "content": 调用栈信息，字符串，格式无要求
}

```
### frame

将当前调用栈设置为指定层次.
```
1. 请求

特殊字段:
{
    "level":栈深度，整数
}

2. 响应

特殊字段:
{
    "content": 调用栈信息，字符串，格式无要求
}

```

# 服务器推送协议

## trigger_breakpoint

触发了一个断点时发送

```
{
   "error": 错误信息，字符串，ok表示成功, 其他表示错误
   "command": "trigger_breakpoint",
   "machine_name":虚拟机名称，字符串
   "unique_id":虚拟机协程唯一ID，整数
   "stack": 当前堆栈信息，字符串，格式无要求
}
```

