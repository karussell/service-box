#pragma once

#if !defined(__linux__)
#define FuncExport extern "C" __declspec(dllexport) // WIN32 DLL exporter
#else
#define FuncExport extern "C" // LINUX shared object exporter
#endif                        // !defined(__linux__)

#include "box_sdk.hh"

/**
 * 获取SDK框架入口接口指针.
 */
FuncExport BoxSDK *get_sdk_frame();

#if !defined(__linux__)
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN // avoid including unnecessary WIN32 header files
#endif
#include <windows.h>
#include <winsock2.h>
#define ModuleHandle HMODULE // DLL handle
#define INVALID_MODULE_HANDLE 0
#define MODULE_SUFFIX ".dll"
#undef max
#else
#include <arpa/inet.h>
#include <dirent.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#define ModuleHandle void * // LINUX SO handle
#define INVALID_MODULE_HANDLE nullptr
#define MODULE_SUFFIX ".so"
#undef max
#endif // !defined(__linux__)

typedef BoxSDK *(*FrameExport)();
#define EXPORT_FUNC_NAME "get_sdk_frame"

//
// 加载框架so并获取框架指针
// \param so_path 框架SO所在路径
// \param 框架指针
// 
static inline BoxSDK *load_and_get_frame(const char *so_path) {
  ModuleHandle handle = INVALID_MODULE_HANDLE;
  FrameExport reg_func = nullptr;
#if defined(_WIN32) || defined(_WIN64)
  handle = ::LoadLibraryA(so_path);
#else
  handle = dlopen(so_path, RTLD_LAZY);
#endif // defined(_WIN32) || defined(_WIN64)
  if (handle == INVALID_MODULE_HANDLE) {
    return nullptr;
  }
#if defined(_WIN32) || defined(_WIN64)
  reg_func = (FrameExport)::GetProcAddress(handle, EXPORT_FUNC_NAME);
#else
  reg_func = (FrameExport)dlsym(handle, EXPORT_FUNC_NAME);
#endif // defined(_WIN32) || defined(_WIN64)
  if (!reg_func) {
    return INVALID_MODULE_HANDLE;
  }
  return reg_func();
}
